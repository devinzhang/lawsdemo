package lawsdemo

import com.itrusign.esp.api.Esp
import com.itrusign.esp.api.EspException
import com.itrusign.esp.api.HttpClientWrapper
import com.itrusign.esp.api.codec.Base64Encoder
import esp.HttpClientConfig
import esp.TestSendAuthCode
import grails.transaction.Transactional
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils
import org.json.JSONObject
import tool.Now
import tool.ResourceUtil
import tool.StreamUtil

import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateEncodingException

@Transactional
class UserService implements HttpClientConfig {
    public static final serviceUrl = "https://esp-test.itrusign.cn/esp";
//    public static final serviceUrl = "http://localhost:8090/esp";
    public static final apiId = "fytx";
    public static final apiSecret = "0ef61b541c74e2a4bc49b15999071d38";
//    public static final apiId = "test";
//    public static final apiSecret  = "23c8b4b87ed2a67db4e076180dcea992";
//
    //xianshang
    public static final orgId = "e3d2c409-ce46-4b40-9b5c-ff4da87cc62f"
//    public static final orgId = "b1143e4e-e4f4-4b77-af48-26e30a17e911"
    //xianshang
    public static final orgPhone = "13606864745"
//    public static final orgPhone = "15606191683"

    public Map<String, Object> createContract(String userId) throws InvalidKeyException, NoSuchAlgorithmException, IOException, CertificateEncodingException, EspException {

        String apiUrl = "contract/create";

        String toSignFileName = "TA3.pdf";
        String title = toSignFileName.substring(0, toSignFileName.lastIndexOf("."));
        String docType = toSignFileName.substring(toSignFileName.lastIndexOf(".") + 1);
        byte[] bytesPdf = StreamUtil.readAllBytes(ResourceUtil.getResource(toSignFileName).openStream());

        Map<String, Object> mapContract = new HashMap<String, Object>();
        mapContract.put("title", title);
        mapContract.put("fileName", toSignFileName);
        mapContract.put("docNum", new Now().toString());
        mapContract.put("doc", new String(new Base64Encoder().encode(bytesPdf)));
        mapContract.put("docType", docType);

        Map<String, Object> reqData = new HashMap<String, Object>();
        reqData.put("apiId", apiId);
        reqData.put("timestamp", System.currentTimeMillis());

        List<Map<String, Object>> listSignatures = new ArrayList<Map<String, Object>>();

        Map<String, Object> mapSignature1 = new HashMap<String, Object>();
        mapSignature1.put("userId", new String(orgId));
        mapSignature1.put("page", 1);
        mapSignature1.put("positionX", 20 + 100);
        mapSignature1.put("positionY", 100);
        listSignatures.add(mapSignature1);

        Map<String, Object> mapSignature2 = new HashMap<String, Object>();
        mapSignature2.put("userId", userId);
        mapSignature2.put("page", 1);
        mapSignature2.put("positionX", 20 + 100 * 2);
        mapSignature2.put("positionY", 100);
        listSignatures.add(mapSignature2);

        mapContract.put("signatures", listSignatures);
        reqData.put("contract", mapContract);
        reqData.put("docRequired", false);

        Esp esp = new Esp(new HttpClientWrapper(), serviceUrl, apiId, apiSecret.getBytes());
        String strJsonRespData = esp.request(apiUrl, new org.json.JSONObject(reqData).toString());
        org.json.JSONObject jsonResp = new org.json.JSONObject(strJsonRespData);

        JSONObject contract = jsonResp.getJSONObject("contract");
        String contractId = contract.getString("contractId");
        String contractname = contract.getString("title");

        Map returnmap = new HashMap();
        returnmap.put("contractName", contractname);
        returnmap.put("contractId", contractId);
        return returnmap;

        System.out.print(jsonResp.toString())

    }

    public boolean singContract(String contractId,String phone,String pUserId) {
        String apiUrl = "contract/sign";
        Map<String, Object> reqData = new HashMap<String, Object>();
        reqData.put("apiId", apiId);
        reqData.put("timestamp", System.currentTimeMillis());

        String userType = "1"
        String userId = orgId
        String mobile = orgPhone

        String authCode = TestSendAuthCode.sendAuthCode(mobile, "contract/sign", userId);
        reqData.put("userId", userId);
        reqData.put("contractId", contractId);
        reqData.put("docRequired", false);
        reqData.put("authCode", authCode);

        Esp esp = new Esp(new HttpClientWrapper(), serviceUrl, apiId, apiSecret.getBytes());
        String strJsonRespData = esp.request(apiUrl, new JSONObject(reqData).toString());
        JSONObject jsonResp = new JSONObject(strJsonRespData);


        Map<String, Object> reqData2 = new HashMap<String, Object>();
        reqData2.put("apiId", apiId);
        reqData2.put("timestamp", System.currentTimeMillis());

        userType = "0"
        userId = pUserId
        mobile = phone

        String authCode2 = TestSendAuthCode.sendAuthCode(mobile, "contract/sign", userId);
        reqData2.put("userId", userId);
        reqData2.put("contractId", contractId);
        reqData2.put("docRequired", false);
        reqData2.put("authCode", authCode2);

        esp = new Esp(new HttpClientWrapper(), serviceUrl, apiId, apiSecret.getBytes());
        String strJsonRespData2 = esp.request(apiUrl, new JSONObject(reqData2).toString());
        JSONObject jsonResp2 = new JSONObject(strJsonRespData2);

        if (jsonResp.getBoolean("isOk") && jsonResp2.getBoolean("isOk")) {
            System.out.println("/contract/sign Ok:\ncontractId=" + contractId);
            return true;
        } else {
            System.err.println("/contract/create Error");
            return false
        }
        return false;
    }

    public String getToken(String userId) {

        String lawsUrl = "http://laws.itrusign.cn/espLoginAttempts"
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(new URI(lawsUrl));
        httpPost.setHeader("ContentType", "application/json");
        JSONObject postData = new JSONObject();

        postData.put("espApiUserId", apiId);
        postData.put("uuid", userId);
        httpPost.setEntity(new StringEntity(postData.toString(), "UTF-8"));
        CloseableHttpResponse response = null;
        String token
        try {
            response = httpclient.execute(httpPost);
            String responseData = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.print(responseData);
            JSONObject jsonResp = new JSONObject(responseData);
            token = jsonResp.getString("token");
            return token
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response.close();
            httpclient.close();
        }
        return null;
    }

    public String createOrder(String token, String contractId) {
        String lawsUrl = "http://laws.itrusign.cn/EspOrder"
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(new URI(lawsUrl));
        httpPost.setHeader("ContentType", "application/json");
        JSONObject postData = new JSONObject();
        postData.put("token", token);
        postData.put("contractId", contractId);
        httpPost.setEntity(new StringEntity(postData.toString(), "UTF-8"));
        CloseableHttpResponse response = null;
        String status;

        try {
            response = httpclient.execute(httpPost);
            String responseData = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.print(responseData);
            JSONObject jsonResp = new JSONObject(responseData);
            status = jsonResp.getString("status");
            return status
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response.close();
            httpclient.close();
        }
        return null;

    }
}