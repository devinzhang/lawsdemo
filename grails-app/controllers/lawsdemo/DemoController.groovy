package lawsdemo

import grails.converters.JSON

class DemoController {
    def userService

    def sign() {
        def reqData = request.JSON
        String userId = reqData.userId
        String needsReport = reqData.needsReport
        String phone = reqData.phone
        Map contractmap = userService.createContract(userId);
        String contractId = contractmap.get("contractId");
        System.out.println("创建合同成功");
        Boolean isOk = userService.singContract(contractId,phone,userId);
        if (!isOk) {
            def respData = [:]
            respData.isOk = false
            respData.message = "合同签署错误"
            respond(respData)
            return
        }
        if (Boolean.valueOf(needsReport)) {
            String token;
            token = userService.getToken(userId);
            String status = userService.createOrder(token, contractId);
            if (status != "created") {
                def respData = [:]
                respData.isOk = false
                respData.message = "见证报告生成错误。"
                render respData as JSON
                return
            }
        }
        def respData = [:]
        respData.isOk = true
        respData.needsReport = needsReport
        respData.contractId = contractId
        respData.userId = userId
        render respData as JSON
    }

    def viewReport() {
        def reqData = request.JSON
        String lawsUrl = reqData.lawsUrl
        String userId = reqData.userId
        String contractId = reqData.contractId
        String token;
        token = userService.getToken(userId);
        String reUrl = lawsUrl + "/#/detail?token=" + token + "&serialNum=" + contractId;

        def respData = [:]
        respData.isOk = true
        respData.reUrl = reUrl
        render respData as JSON


        def ret = [isOk: true, rtrul: reUrl]
        render ret as JSON

    }
}
