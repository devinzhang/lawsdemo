package esp;

import com.itrusign.esp.api.Esp;
import com.itrusign.esp.api.EspException;
import com.itrusign.esp.api.HttpClientWrapper;
import org.codehaus.groovy.grails.web.json.JSONObject;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class TestSendAuthCode implements HttpClientConfig {

    public static String sendAuthCode(String mobile, String usage, String userId)
            throws InvalidKeyException, NoSuchAlgorithmException, IOException, EspException {

        String apiUrl = "authCode/send";
        String serviceUrl = "https://esp-test.itrusign.cn/esp";
//        String serviceUrl = "http://localhost:8090/esp";
        String apiId = "fytx";
        String apiSecret = "0ef61b541c74e2a4bc49b15999071d38";
        Map<String, Object> reqData = new HashMap<String, Object>();
        reqData.put("apiId", apiId);
        reqData.put("timestamp", System.currentTimeMillis());

        reqData.put("usage", usage);
        reqData.put("mobile", mobile);
        if (userId != null) {
            reqData.put("userId", userId);
        }

        Esp esp = new Esp(new HttpClientWrapper(), serviceUrl, apiId, apiSecret.getBytes());
        String strJsonRespData = esp.request(apiUrl, new JSONObject(reqData).toString());
        JSONObject jsonResp = new JSONObject(strJsonRespData);

        if (jsonResp.getBoolean("isOk")) {
            String authCode = jsonResp.getString("authCode");
            System.out.println("/authCode/send Ok:\nauthCode=" + authCode);
            return authCode;
        } else {
            System.err.println("/authCode/send Error:\n" + strJsonRespData);
            return null;
        }
    }

    public static void main(String[] args) throws Exception {
        sendAuthCode("13241001234", "user/create", null);
    }

}
