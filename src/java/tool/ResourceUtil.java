package tool;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class ResourceUtil {
    public static URL getResource(final String name) {
        URL resource;
        resource = ResourceUtil.class.getClassLoader().getResource(name);

        if (resource == null) {
            try {
                resource = new File("resources/" + name).toURI().toURL();
            } catch (MalformedURLException e) {
                throw new RuntimeException("can not find resource " + name);
            }
        }
        return resource;
    }
}
