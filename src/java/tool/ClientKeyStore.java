package tool;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

public class ClientKeyStore {
    private final static String JKS_RESOURCE_NAME = "ClientCert.jks";
    private final static char[] password = "password".toCharArray();
    private static KeyStore keyStore;
    private static String alias;

    static {
        try {
            keyStore = KeyStore.getInstance("JKS");
            keyStore.load(ResourceUtil.getResource(JKS_RESOURCE_NAME).openStream(), password);
            alias = keyStore.aliases().nextElement();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Certificate getCertificate() {
        try {
            return keyStore.getCertificate(alias);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PrivateKey getPrivateKey() {
        try {
            return (PrivateKey) keyStore.getKey(alias, password);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void storeToP12() {
        try {
            KeyStore store = KeyStore.getInstance("PKCS12");
            store.load(null);
            store.setKeyEntry(alias, keyStore.getKey(alias, password), password, keyStore.getCertificateChain(alias));
            store.store(new FileOutputStream("ClientCert.pfx"), password);
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        storeToP12();
    }
}
